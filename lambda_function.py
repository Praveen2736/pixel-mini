import boto3
import PIL
from PIL import Image
from io import BytesIO
import sys
import os
from os import path

s3 = boto3.resource('s3')
client = boto3.client('s3')

origin_bucket   = os.environ['ORIGIN_BUCKET'] 
target_bucket   = os.environ['TARGET_BUCKET']

thumnail_version = (640, 480)
thumbnail_version_extension = 'large'

# event = {'Records': [{

#   'eventVersion': '2.1', 
#   'eventSource': 'aws:s3', 
#   'awsRegion': 'us-east-1', 
#   'eventTime': '2018-12-23T18:27:31.991Z', 
#   'eventName': 'ObjectCreated:Post', 
#   'userIdentity': {'principalId': 'A3RPVOSC19EXS9'}, 
#   'requestParameters': {'sourceIPAddress': '150.242.64.255'}, 
#   'responseElements': {'x-amz-request-id': 'ECAAA9EF1F0AB887', 'x-amz-id-2': 'hnbNN9vX6lsYz5MVZEYdPyEeuGL25WkfJM70tiJeaXH4Ro9R3caHT0X+rrVMItM2uxy+sFAbX7s='}, 
#    's3': {  
#         's3SchemaVersion': '1.0', 
#         'configurationId': '5461b0f1-34d1-4af4-b1ec-eff27098d7e7', 
#         'bucket': {
#             'name': 'imagealbumupload', 
#             'ownerIdentity': {'principalId': 'A3RPVOSC19EXS9'}, 
#             'arn': 'arn:aws:s3:::images-album-3'
#           }, 
#          'object': {
#             'key': 'imagealbumupload/e7079b63/1545902329725-fdhsshg4r5s/original.jpg', 
#             'size': 40878, 
#             'eTag': '7c4644d8efb399b1b5bdc1c0fa1e95ec', 
#             'versionId': 'X55fngjddKYNrF1xT6eJOJi0MAg8xmZg', 
#             'sequencer': '005C1FD39186B58A78'
#           }
#         }
#     }
#     ]
#   }


def lambda_handler(event, context):
  object_key = event["Records"][0]['s3']['object']['key'].replace(origin_bucket+'/', '')
  extension = object_key.split(".").pop().lower()
  s3_data = get_s3_object(object_key)
  new_object_key = object_key.replace('original', thumbnail_version_extension)
  original_img = Image.open(BytesIO(s3_data))
  original_img = reorient_image(original_img)
  buffer = BytesIO()
  original_img.thumbnail(thumnail_version, Image.ANTIALIAS)
  if extension == 'jpg':
    extension = 'jpeg'
  original_img.save(buffer, extension,  quality=85,optimize=True, progressive=True)
  client.put_object(Bucket = target_bucket, Body = buffer.getvalue(), Key = new_object_key, ContentType = "image/" + extension)


def reorient_image(im):
    try:
        image_exif = dict(im._getexif().items())
        image_orientation = image_exif[274]
        if image_orientation in (2,'2'):
            return im.transpose(Image.FLIP_LEFT_RIGHT)
        elif image_orientation in (3,'3'):
            return im.transpose(Image.ROTATE_180)
        elif image_orientation in (4,'4'):
            return im.transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (5,'5'):
            return im.transpose(Image.ROTATE_90).transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (6,'6'):
            return im.transpose(Image.ROTATE_270)
        elif image_orientation in (7,'7'):
            return im.transpose(Image.ROTATE_270).transpose(Image.FLIP_TOP_BOTTOM)
        elif image_orientation in (8,'8'):
            return im.transpose(Image.ROTATE_90)
        else:
            return im
    except (KeyError, AttributeError, TypeError, IndexError):
        return im

def get_s3_object(object_key):
  obj = s3.Object(bucket_name=origin_bucket,key=object_key)
  obj_body = obj.get()['Body'].read()
  return obj_body


