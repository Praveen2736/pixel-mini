# PIXEL-MINI

*Python Script to generate Thumbnails of image*


#Usage

 - This entire package has to be zipped and uploaded to AWS Lambda.

 - Whenever a image is uploaded to S3. Lambda will trigger the **lambda_function.py/lambda_handler** 

#Library used

 - To generate thumbnail version **PIL/Image** module will be used. 


# Rotation Issue

 - To handle the rotation issue of thumbnail image exif data from image will be used (refer http://www.exiv2.org/tags.html,   https://www.daveperrett.com/articles/2012/07/28/exif-orientation-handling-is-a-ghetto/ ).
    Exif tag 274 has the imageorientation data.

- Before generation thumbnails we will be correcting the orientation using this method **lambda_function.py/reorient_image**



